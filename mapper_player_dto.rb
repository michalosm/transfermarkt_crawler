class MapperPlayerDto
  def self.convert_list_to_list_player_dto(list = [])
    list.map { |el| PlayerDto.new(el[:name], el[:position], el[:market_value]) }
  end
end
