class PlayerDto
  attr_accessor :name, :position, :market_value

  def initialize(name, position, market_value)
    self.name = name
    self.position = position
    self.market_value = market_value
  end

  def to_json(options = {})
    { "PlayerName" => self.name, "Position" => self.position,
      "MarketValue": self.market_value }.to_json
  end

  def to_s
    "#{name} #{position} #{market_value}"
  end
end
