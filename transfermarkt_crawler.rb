require "nokogiri"
require "httparty"
require "pry"
require_relative "./player_dto"
require_relative "./mapper_player_dto"

class TransfermarktCrawler
  attr_accessor :url

  def initialize(url)
    self.url = url
  end

  def scrape_players_data
    results = process_table_rows(parse.css("table.items tbody tr.odd"))
    results + process_table_rows(parse.css("table.items tbody tr.even"))
  end

  private

  def parse
    unparsed_page = HTTParty.get(url)
    Nokogiri::HTML(unparsed_page)
  end

  def process_table_rows(document)
    document.map do |result|
      begin
        position = result.children[1].attributes["title"].value
        name = result.children[3].children.text
        market_value = result.children[6].children.text.gsub(/[[:space:]]/, "")
        { name: name, position: position, market_value: market_value }
      rescue
        puts "Error, probably structure of webpage changed"
      end
    end
  end
end

if __FILE__ == $0
  t = TransfermarktCrawler.new("https://www.transfermarkt.com/legia-warschau/startseite/verein/255/saison_id/2019")
  result = MapperPlayerDto.convert_list_to_list_player_dto(t.scrape_players_data)
  directory_name = "results"
  Dir.mkdir(directory_name) unless File.exists?(directory_name)
  time = Time.now
  file_name = time.strftime("%m-%d-%Y.%H.%M.%S")
  File.open("#{directory_name}/#{file_name}.json", "w+") do |f|
    f.write(result.to_json)
  end
end
