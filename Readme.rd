Version 0.1 Crawling https://www.transfermarkt.com
At this time, there is only a subpage option: https://www.transfermarkt.com/legia-warschau/startseite/verein/255/saison_id/2019

To run the script you must install the gems from the Gemfile file:
gem 'httparty', '~> 0.13.7'
gem 'nokogiri'
gem 'pry' (only for debugging)

Running the crawling script:
ruby transfermarkt_crawler.rb

As a result, a json file should be created in the results directory.
A new file is created each time you start.